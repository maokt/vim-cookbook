vim Cookbook
============

Install vim and the vimoutliner plugin.

Also provides a `vim_addon` resource to install or remove the addons from the system vim configuration.

Requirements
------------

Works on Ubuntu and probably other Debian distributions.

Uses the vim-addon-manager package by Stefano Zacchiroli and Antonio Terceiro.

Usage
-----

First, include `recipe[vim]` in your node's `run_list`.

The `vim_addon` resource is simple. The resource name is the addon name.
Available actions are `:install` (the default) and `:remove`.

```ruby
vim_addon "vimoutliner"
```

