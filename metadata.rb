name             'vim'
version          '0.1.0'
description      'Installs/Configures vim and vim addons'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
license          'GPL v3'
maintainer       'Marty Pauley'
maintainer_email 'marty+chef@martian.org'
supports         'ubuntu'
