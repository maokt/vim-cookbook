def whyrun_supported?
    true
end

action :install do
    if @current_resource.status == :installed
        Chef::Log.info " #{@new_resource} already installed"
    else
        converge_by("install #{@new_resource}") do
            vam.send :install, [ @@registry[@new_resource.name] ]
        end
    end
end

action :remove do
    if @current_resource.status == :not_installed
        Chef::Log.info " #{@new_resource} is not installed"
    else
        converge_by("remove #{@new_resource}") do
            vam.send :remove, [ @@registry[@new_resource.name] ]
        end
    end
end

def load_current_resource
    load_vam
    load_registry
    name = @new_resource.name
    @current_resource = Chef::Resource::VimAddon.new(name)
    @current_resource.name(name)
    if @@registry[name]
        @current_resource.status = @@registry[name].status(@@vam_system_dir).status
    else
        # not sure if this is the best place to fail, but I will fail here anyway
        raise %Q{unknown vim addon #{@current_resource}}
    end
end

def load_vam
    vpath = "/usr/lib/ruby/vendor_ruby"
    $:.push(vpath) unless $:.include?(vpath)
    require "vim/addon_manager"
    require "vim/addon_manager/registry"
    Vim::AddonManager.logger.quiet!
    @@vam_system_dir   = '/var/lib/vim/addons'
end

def load_registry
    registry_dir = '/usr/share/vim/registry'
    source_dir   = '/usr/share/vim/addons'
    @@registry = Vim::AddonManager::Registry.new(registry_dir, source_dir)
end

def vam
    Vim::AddonManager.new @@vam_system_dir
end
